﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Newtonsoft.Json;// Add package for JSON
using System.IO; // Read/write files

namespace WPF_JSON_Demo
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private LegoSet mySet;
        private List<LegoSet> ListOfSets;

        public MainWindow()
        {
            InitializeComponent();
            InitialiseSeriesCombo();

            // Initialise the list of sets
            ListOfSets = new List<LegoSet>();

            InitialiseListView();
        }

        private void InitialiseSeriesCombo()
        {
            cboSeries.Items.Add("Creator");
            cboSeries.Items.Add("Technic");
            cboSeries.Items.Add("Friends");
            cboSeries.Items.Add("Duplo");
            cboSeries.Items.Add("Creator");
            cboSeries.Items.Add("Star Wars");
            cboSeries.Items.Add("Harry Potter");
            cboSeries.Items.Add("Speed Demons");
            cboSeries.Items.Add("Ninjago");
        } // end default combo box values

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            mySet = new LegoSet();
            mySet.Title = tbSetTitle.Text;
            mySet.SetNumber = tbSetNumber.Text;
            mySet.Picture = tbPicture.Text;
            mySet.Series = cboSeries.SelectedValue.ToString();
            mySet.Quantity = Convert.ToInt32(tbQuantity.Text);
            mySet.Wanted = Convert.ToBoolean(chkWanted.IsChecked);
            mySet.Owned = Convert.ToBoolean(chkOwned.IsChecked);

            ListOfSets.Add(mySet);
        } // end save to object

        private void btnSerialize_Click(object sender, RoutedEventArgs e)
        {
            // serialize JSON directly to a file
            using (StreamWriter file = File.CreateText(@"lego-sets.json"))
            {
                JsonSerializer serializer = new JsonSerializer();
                serializer.Serialize(file, ListOfSets);
            } // end using Stream writer
        } // end serialise and save

        private void btnDeserialize_Click(object sender, RoutedEventArgs e)
        {
            using (StreamReader file = File.OpenText(@"lego-sets.json"))
            {
                String jsonText = File.ReadAllText(@"lego-sets.json");
                ListOfSets = JsonConvert.DeserializeObject<List<LegoSet>>(jsonText);
            } // end stream read

            InitialiseListView();
        }

        private void InitialiseListView()
        {
            // Add columns
            var gridView = new GridView();
            lvSets.View = gridView;

            gridView.Columns.Add(new GridViewColumn
            {
                Header = "Title",
                DisplayMemberBinding = new Binding("Title")
            });
            gridView.Columns.Add(new GridViewColumn
            {
                Header = "Set No",
                DisplayMemberBinding = new Binding("SetNumber")
            });
            gridView.Columns.Add(new GridViewColumn
            {
                Header = "Series",
                DisplayMemberBinding = new Binding("Series")
            });
            gridView.Columns.Add(new GridViewColumn
            {
                Header = "Picture",
                DisplayMemberBinding = new Binding("Picture")
            });
            gridView.Columns.Add(new GridViewColumn
            {
                Header = "Wanted",
                DisplayMemberBinding = new Binding("IsWanted")
            });
            gridView.Columns.Add(new GridViewColumn
            {
                Header = "Owned",
                DisplayMemberBinding = new Binding("IsOwned")
            });
            gridView.Columns.Add(new GridViewColumn
            {
                Header = "Quantity",
                DisplayMemberBinding = new Binding("Quantity")
            });
            lvSets.ItemsSource = ListOfSets;
        }

    } // end Main Window class

} // end namespace
