﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPF_JSON_Demo
{
    class LegoSet
    {
        private String title;
        private String setNumber;
        private String series;
        private String picture;
        private Boolean own;
        private Boolean want;
        private int quantity;

        /* Getters and Setters */

        public String Title
        {
            get { return title; }
            set { title = value; }
        }

        public String SetNumber
        {
            get { return setNumber; }
            set { setNumber = value; }
        }

        public String Series
        {
            get { return series; }
            set { series = value; }
        }

        public String Picture
        {
            get { return picture; }
            set { picture = value; }
        }

        public Boolean Owned
        {
            get { return own; }
            set { own = value; }
        }

        /// <summary>
        /// Do we own this set?
        /// </summary>
        /// <returns>boolean</returns>
        public Boolean IsOwned()
        {
            return Owned;
        }

        public Boolean Wanted
        {
            get { return want; }
            set { want = value; }
        }

        public Boolean IsWanted()
        {
            return Wanted;
        }

        public int Quantity
        {
            get { return quantity; }
            set {
                if (value >= 0)
                {
                    quantity = value;
                } else
                {
                    quantity = 0;
                } // end if value...
            } // end set
        } // end get/set Quantity

    } // end class LegoSet

} // end namespace WPF_JASON_Demo
